const messages = {
    200: "sukses",
    201: "data berhasil disimpan",
    400: "not found"
  };
  function successResponse(res, code, data, meta = {}) {
    res.status(code).json({
      data,
      meta: {
        code,
        message: messages[code.toString()],
        ...meta,
      },
    });
  }
  
  module.exports = { successResponse };
