const { successResponse } = require('../../helpers/response');
const { User, Game } = require('../../models');

class HomeController {
  getGame = (req, res) => {
    Game.findAll()
      .then((games) => {
        res.json(successResponse(res, 200, games));
      })
      .catch('Games Not Found');
  };

  getDetailGame = (req, res) => {
    Game.findOne({
      where: { id: req.params.id },
    })
      .then((game) => {
        res.json(successResponse(res, 200, game));
      })
      .catch('Detail Game not Found');
  };

  updateScore = (req, res) => {
    User.update(
      {
        total_score: req.body.total_score
      },
      {
        where: { id: req.params.id }
      })
      .then((game) => {
        res.json(successResponse(res, 200, game));
      })
      .catch('Failed to Update');
  };

  startGame = (req, res) => {
    Game.update(
      {
        play_count: req.body.play_count
      },
      {
        where: { id: req.params.id }
      } 
    )
      .then((game) => {
        res.json(successResponse(res, 200));
      })
      .catch('Failed to Update');
  };
}

module.exports = HomeController;
