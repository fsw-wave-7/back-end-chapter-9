const { User } = require("../../models")
const { successResponse } = require("../../helpers/response")
const bcrypt = require("bcrypt")
const passport = require("../../lib/passport-jwt")

function format(user) {
    const { id, username } = user
    return {
        id,
        username,
        accessToken: user.generateToken()
    }
}
class AuthController {
    register = (req, res) => { 
        User.register(req.body) 
            .then((user) => {
            res.json(successResponse(res, 201, user))
            })
            .catch("fail")
    }

    login = (req, res) => {
        User.authenticate(req.body)
            .then((user) => {
            res.json(format(user))
            })
            .catch((err)=> res.send(err))
    } 

    whoami = (req, res) => {
        res.send(req.user.dataValues);
    };

    logout = (req, res) => {
        res.clearCookie('loginData')
            res.json(successResponse(res, 200))
    }
}

module.exports = AuthController