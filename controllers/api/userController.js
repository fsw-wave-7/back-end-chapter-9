const { User } = require('../../models')
const { successResponse } = require("../../helpers/response")
const bcrypt = require('bcrypt')

class UserController {
    
// USER
getUser = (req,res) => {
  User.findAll()
  .then((user) => {
    res.json(successResponse(res,200,user))
  })
.catch('User Not Found')
}
    
getDetail = (req,res) => {
    User.findOne({
        where: { id: req.params.id},
    })
    .then((user) => {
        res.json(successResponse(res, 200,user))
    })
    .catch('fail')
}

  editUser = (req, res) => { 
    const updatePassword = bcrypt.hashSync(req.body.password, 10)
    User.update(
      {
        email:req.body.email,
        username: req.body.username,
        password: updatePassword,
        bio: req.body.bio,
        city: req.body.city,
        social_media_url: req.body.social_media_url,
      },
      {
        where: { id: req.params.id },
      })
      .then((user) => {
        res.json(successResponse(res, 201));
      })
      .catch('fail')
  };


  deleteUser = (req, res) => {
    User.destroy({
      where: {
        id: req.params.id,
      },
    })
    .then(() => {
      res.json(successResponse(res, 200, null));
    });
  };

  

}

module.exports = UserController;
