'use strict';

module.exports = {
  up:  (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert("Games", [{
      name: 'Batu,Gunting,Kertas',
      description: 'Game Tradisional',
      thumbnail_url: 'dummy',
      game_url: 'dummy',
      play_count: 1,
      createdAt: new Date(),
      updatedAt: new Date()
     }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
