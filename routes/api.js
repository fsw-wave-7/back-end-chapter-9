var express = require('express');
var router = express.Router();
const AuthController = require('../controllers/api/authController');
const HomeController = require('../controllers/api/homeController');
const UserController = require('../controllers/api/userController');
const authController = new AuthController();
const homeController = new HomeController();
const userController = new UserController();
const { Pool } = require('pg');
const pool = new Pool({
  connectionString: process.env.DATABASE_URL,
  ssl: {
    rejectUnauthorized: false,
  },
});
const restrict = require('../middleware/restrict-api');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

// USER
router.post('/register', authController.register);
router.post('/login', authController.login);


router.post('/logout', authController.logout);

/// USER
router.get('/user', restrict, userController.getUser);
router.get('/user/:id', restrict, userController.getDetail);
router.put('/user/:id', restrict, userController.editUser);
router.delete('/user/:id', restrict, userController.deleteUser);

// HOME
router.get('/game', homeController.getGame);
router.get('/game/:id', homeController.getDetailGame);
router.post('/score/:id', homeController.updateScore);
router.post('/play/:id', homeController.startGame);

router.get('/db', async (req, res) => {
  try {
    const client = await pool.connect();
    const result = await client.query('SELECT * FROM test_table');
    const results = { results: result ? result.rows : null };
    res.send('db');
    client.release();
  } catch (err) {
    console.error(err);
    res.send('Error ' + err);
  }
});

module.exports = router;
