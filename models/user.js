'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  class User extends Model {

    static associate(models) {
      // define association here
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10)

    static register = ({ email, username, password }) => {
      const encryptedPassword = this.#encrypt(password)
      return this.create({ email, username, password: encryptedPassword })
    }

    checkPassword = password => bcrypt.compareSync(password, this.password)
    
    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username }})
        if (!user) return Promise.reject("User not found!")
        const isPasswordValid = user.checkPassword(password)
        if (!isPasswordValid) return Promise.reject("Wrong password")
        return Promise.resolve(user)
      }
      catch(err) {
        return Promise.reject(err)
      }
    }

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username
      }
      const rahasia = 'inirahasia'
      const token = jwt.sign(payload, rahasia)
      return token
    }
  };
  User.init({
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    total_score: DataTypes.INTEGER,
    bio: DataTypes.STRING,
    city: DataTypes.STRING,
    social_media_url: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};
